<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Eintrag;
use AppBundle\Form\Type\SearchForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    private function createSearchForm()
    {
        $form = $this->createForm(SearchForm::class, null, ["action" => $this->generateUrl("view_route")]);
        return $form;
    }
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createSearchForm();
        $form ->add('Suchen',SubmitType::class);
        return $this->render('Form/EintragForm.html.twig', ['formdata' => $form->createView()]);
    }
    /**
     * @Route("/view/", name="view_route")
     */
    public function viewDataAction(Request $request)
    {
        $searchResult =$this->getDoctrine()->getRepository(Eintrag::class)->findSearchedEintrag(null);
        $form = $this->createSearchForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $searchResult =$this->getDoctrine()->getRepository(Eintrag::class)->findSearchedEintrag($form->getData());
        }
        return $this->render('Form/SearchResult.html.twig',['searchResult' => $searchResult]);
    }
}
