<?php
/**
 * Created by PhpStorm.
 * User: Osama Sado
 * Date: 8/1/2017
 * Time: 10:07 PM
 */

namespace AppBundle\Command;


use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMNodeList;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use AppBundle\Entity\Eintrag;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends ContainerAwareCommand
{
    private $entityManager;
    private $xmlFile;
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->entityManager = $em;
    }

    /**
     * @var string
     */
    private $commandName = 'app:import';

    /**
     * Sets up command informations.
     */
    protected function configure()
    {
        $this->setName($this->commandName)
            ->setDescription('Import data from the import.xml file.')
            ->setDefinition([])
            ->setHelp(<<<EOT
                        The <info>{$this->commandName}</info> import data from the import.xml file
                        <info>php bin/console {$this->commandName}</info>
EOT
            );
    }
    /**
     * @return DOMDocument
     */
    protected function loadXML()
    {
        $filePath = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import.xml';
        //$filePath = "https://raw.githubusercontent.com/BSData/wh40k/master/Imperium%20-%20Space%20Wolves.cat";
        $xmlDoc = new DOMDocument();
        $xmlDoc->load( $filePath);
        $xmlFile = $xmlDoc->getElementsByTagName('selectionEntry');
        $this->xmlFile = $xmlFile;
    }
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadXML();
        $this->importData($this->xmlFile);
        $output->writeln('Import successful.');
    }
    /**
     * @param DOMNodeList $xmlFile all selectionEntries in Xml file
     */
    protected function importData(DOMNodeList $xmlFile)
    {
        for ($i = 0; $i < $xmlFile->length; ++$i)
        {
            $item = $xmlFile->item($i);
            if($item->hasAttributes())
            {
                $eintrag=$this->entityManager->getRepository('AppBundle:Eintrag')->find($i+1);
                if($eintrag==null)
                    $eintrag = new Eintrag();
                $eintrag->setId($i+1);
                $parent = $this->findParent($i);
                $eintrag->setParent($parent);
                $eintrag->setName($item->getAttribute('name'));
                $eintrag->setType($item->getAttribute('type'));
            }
            $constraintValue = $this->addConstraint($i);
            $costValue = $this->addCost($i);
            $eintrag->setEConstraint( $constraintValue);
            $eintrag->setCost($costValue*$constraintValue);
            if($parent != null)
            {
                $parent->setCost($parent->getCost()+($costValue*$constraintValue));
            }
            $this->entityManager->persist($eintrag);
        }
        $this->entityManager->flush();
    }
    protected function findParent($index)
    {

        $item = $this->xmlFile->item($index);
        $idparent = 0;
        $currentPath=$item->getNodePath();
        for ($j = 0; $j <$this->xmlFile->length; ++$j)
        {
            $tempItem =$this->xmlFile->item($j);
            $tempPath = $tempItem->getNodePath();
            $xPath = substr($currentPath,strlen($tempPath),32);
            $yPath = substr($currentPath,0,strlen($tempPath));
            if(($xPath == "/selectionEntries/selectionEntry") and ($yPath==$tempPath))
            {
                $idparent =$j+1;
                break;
            }
        }
        if($idparent==0)
            $parent=null;
        else
        {
            $parent=$this->entityManager->getRepository('AppBundle:Eintrag')->find($idparent);
        }
        return  $parent;
    }
    protected function addCost($index)
    {
        $item =$this->xmlFile->item($index);
        $costs = $item->getElementsByTagName( "cost" );
        $costValue = 0;
        for ($j = 0; $j < $costs->length; ++$j)
        {
            $cost = $costs->item($j);
            if($cost->hasAttributes())
            {
                if($cost->getAttribute('name')=='pts')
                {
                    $costPath = $cost->getNodePath();
                    $itemPath = $item->getNodePath();
                    $xtemp = substr($costPath,strlen($itemPath),11);
                    $ytemp = substr($costPath,0,strlen($itemPath));
                    if(($xtemp=='/costs/cost') and ($ytemp==$itemPath))
                    {
                        $costValue = $cost->getAttribute('value');
                    }
                }
            }
        }
        return $costValue;
    }
    protected function addConstraint($index)
    {
        $item = $this->xmlFile->item($index);
        $constraints = $item->getElementsByTagName( "constraint" );
        $constraintValue = 1;
        for ($j = 0; $j < $constraints->length; ++$j)
        {
            $constraint = $constraints->item($j);
            if($constraint->hasAttributes())
            {
                if($constraint->getAttribute('type')=='min')
                {
                    $constaintPath = $constraint->getNodePath();
                    $itemPath = $item->getNodePath();
                    $xtemp = substr($constaintPath,strlen($itemPath),23);
                    $ytemp = substr($constaintPath,0,strlen($itemPath));
                    if(($xtemp=='/constraints/constraint') and ($ytemp==$itemPath))
                    {
                        $constraintValue = $constraint->getAttribute('value');
                    }
                }
            }
        }
        return $constraintValue;
    }
}