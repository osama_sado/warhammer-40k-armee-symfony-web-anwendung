<?php
/**
 * Created by PhpStorm.
 * User: Osama Sado
 * Date: 7/31/2017
 * Time: 8:43 PM
 */

namespace AppBundle\Form\Type;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchForm extends AbstractType
{
    private $TypeList;

    /**
     * SearchForm constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->TypeList= $em->getRepository('AppBundle:Eintrag')->getTypeList();
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('PunkteOp',ChoiceType::class, array('required'   => false,'label'=>'Punkte Option',
                'choices' => array(
                    'Mehr als' => '>=',
                    'Weniger als' => '<=',
                    'Gleich' => '=',
                )))
            ->add('Zahl',NumberType::class,array('required'   => false, 'empty_data' => '0','data' => '0' ))
            ->add('Type',ChoiceType::class, array('required'   => false,
                'choices' =>$this->TypeList,
            ))
            ->add('Name',TextType::class,array('required'   => false ))
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
        ));
    }
}