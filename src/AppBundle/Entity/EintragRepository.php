<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;


class EintragRepository extends EntityRepository
{

    public function getParentEintrag()
    {
        $subQuery = $this->getEntityManager()->createQueryBuilder();
        $subQuery->select('IDENTITY(p.parent)');
        $subQuery->from('AppBundle:Eintrag', 'p');
        $qr = $subQuery->getQuery();
        $parents= $qr->getResult();
        return $parents;
    }
    public function getNonChildParentEintrag()
    {
        $parents= $this->getParentEintrag();
        $subQuery = $this->getEntityManager()->createQueryBuilder();
        $subQuery->select('e.id');
        $subQuery->from('AppBundle:Eintrag', 'e');
        $subQuery->where($subQuery->expr()->notIn('e.id', ':parents'));
        $subQuery->setParameter('parents', $parents);
        $subQuery->orWhere('e.parent  IS NULL');
        $qr = $subQuery->getQuery();
        $nonChildparents= $qr->getResult();
        return $nonChildparents;
    }
    public function findSearchedEintrag($searchOptions)
    {
        $parents= $this->getParentEintrag();
        $nonChildparents= $this->getNonChildParentEintrag();
        $queryBuilder =$this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('e');
        $queryBuilder->from('AppBundle:Eintrag', 'e');
        $queryBuilder->where($queryBuilder->expr()->in('e.id', ':parents'));
        $queryBuilder->setParameter('parents', $parents);
        $queryBuilder->orWhere($queryBuilder->expr()->in('e.id', ':nonChildparents'));
        $queryBuilder->setParameter('nonChildparents', $nonChildparents);
        if($searchOptions['Name'])
        {
            $queryBuilder->andWhere('e.name = :name');
            $queryBuilder->setParameter('name', $searchOptions["Name"]);
        }
        if($searchOptions['Type'])
        {
            $queryBuilder->andWhere('e.type = :type');
            $queryBuilder->setParameter('type', $searchOptions["Type"]);
        }
        if($searchOptions['PunkteOp'])
        {
            $queryBuilder->andWhere('e.cost '.$searchOptions['PunkteOp'].' :zahl');
            $queryBuilder->setParameter('zahl', $searchOptions['Zahl']);
        }
        $query = $queryBuilder->getQuery();
        $searchResult= $query->getResult();
        return $searchResult;
    }
    public function getTypeList()
    {
        $queryBuilder =  $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('e.type');
        $queryBuilder->from('AppBundle:Eintrag','e');
        $queryBuilder->distinct();
        $queryBuilder ->orderBy('e.type','ASC');
        $query = $queryBuilder->getQuery();
        $types= $query->getResult();
        $typesArray=[];
        for($i=0;$i<count($types);$i++)
            $typesArray[$types[$i]["type"]]=$types[$i]["type"];
        return $typesArray;
    }
}
