<?php
/**
 * Created by PhpStorm.
 * User: Osama Sado
 * Date: 7/26/2017
 * Time: 10:55 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="EintragRepository")
 * @ORM\Table(name = "Eintrag")
 * @package AppBundle\Entity
 */

class Eintrag
{
    /**
     * One Eintrg has Many Untereintrage.
     * @ORM\OneToMany(targetEntity="Eintrag", mappedBy="parent")
     */
    private $children;
    /**
     * Many Untereintrage have One Eintrage as Parent.
     * @ORM\ManyToOne(targetEntity="Eintrag", inversedBy="children")
     * @ORM\JoinColumn(name="id_parent", referencedColumnName="id")
     */
    private $parent;

    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $id;
    /**
     * @ORM\Column(type="string",length =100)
     */
    private $name;
    /**
     * @ORM\Column(type="string",length =100)
     */
    private $type;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $cost;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $econstraint;
    /**
     * @return Int Id von einem Eintrag
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return String Name von einem Eintrag
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return String Type von einem Eintrag
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Decimal Cost von einem Eintrag
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return Decimal constraint von einem Eintrag
     */
    public function getEconstraint()
    {
        return $this->econstraint;
    }

    /**
     * @param $econstraint
     */
    public function setEconstraint($econstraint)
    {
        $this->econstraint = $econstraint;
    }

    /**
     * @return Eintrag ObjectParent von einem Eintrag
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param  $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
    /**
     * @return Array of Objects als Einträge kinder von einem Eintrag
     */
    public function getChildren()
    {
        return $this->children;
    }
}